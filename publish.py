#!/usr/bin/env python3
import subprocess
import argparse
import re

parser = argparse.ArgumentParser()
parser.add_argument("version")
parser.add_argument("-f", "--force", action="store_true")
args = parser.parse_args()
if (
    not args.force and subprocess.call(["git", "diff-index", "--quiet", "HEAD"]) != 0
):  # noqa
    raise RuntimeError("Working directory must be clean.")
if not re.match("\\d+\\.\\d+\\.\\d+", args.version):
    args.error("version must be in the format N.N.N")
subprocess.check_call(["poetry", "version", args.version])
subprocess.check_call(
    [
        "git",
        "commit",
        "pyproject.toml",
        "--allow-empty",
        "-m",
        "Version {}".format(args.version),
    ]
)
subprocess.check_call(["git", "tag", "v{}".format(args.version)])
subprocess.check_call(["git", "push"])
subprocess.check_call(["git", "push", "--tags"])
subprocess.check_call(["poetry", "build"])
subprocess.check_call(
    [
        "poetry",
        "publish",
        "-u",
        "rendaw",
        "--password",
        subprocess.check_output(["gopass", "show", "service/pypi/rendaw"])
        .decode("utf-8")
        .strip(),
    ]
)
